extends "res://addons/gut/test.gd"


func before_all():
    pass
    
func test_dedicated_host_game_state_is_one():
    assert_eq(PM.SS["GameCommon"].game_state_by_mup[PM.unique_id]
        , 1, "Dedicated Host should only be at a game state of 1."
    )
    

