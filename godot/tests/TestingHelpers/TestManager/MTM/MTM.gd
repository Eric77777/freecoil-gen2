extends Node

var time_last = OS.get_ticks_msec()
var has_failed = false
var Output: Label

onready var Gut = get_node("Control/Gut")

# Called when the node enters the scene tree for the first time.
func _ready():
    OS.set_exit_code(125)  # Set exit code to a failure until the tests are complete.
    print("MTM Launched.")
        
func launch_clients(options):
    if PM.ITM_allow_launch_clients:
        var all_options = ["--path", ProjectSettings.globalize_path("res://")]
        for option in options:
            all_options.append(option)
#        ["--path", ProjectSettings.globalize_path("res://"), 
#            "--ITM_act_as=client", "ITM_clear_user_dir=true"
#        ]
        # warning-ignore:return_value_discarded
        OS.execute(OS.get_executable_path(), all_options, false)
        
func auto_press_unpress_event(event):
    Input.parse_input_event(event)
    var unpress = event.duplicate()
    unpress.pressed = false
    call_deferred("simulate_unpress", unpress)

func simulate_unpress(event):
    Input.parse_input_event(event)

func _on_state_management_finished_loading():
    Output = get_tree().root.get_node("Container/Scene1/Empty/MasterTestManager/" +
        "CenterContainer/VBoxContainer/VBoxContainer2/ScrollContainer/Description"
    )
    Output.text = "Finished state management loading."
    call_deferred("run_all_unit_tests")

func _on_Gut_gut_ready():
    Gut.export_if_tests_found()
    Gut.import_tests_if_none_found()
    print("MTM: GUT Ready.")
    Gut._gut.rect_size.y = 300
    
func run_tests(script_path):
    Gut.get_gut().test_script(script_path)
    
func run_all_unit_tests():
    Output.text = "Running All Unit Tests."
    Gut.get_gut().test_scripts()

func _on_Gut_tests_finished():
    Gut._gut.rect_size.y = 300
    Output.text = "All unit tests completed."
    var totals = Gut._gut.get_summary().get_totals()
    print(totals)
    if(totals["passing"] + totals["failing"] == 0 or totals["scripts"] ==  0):
        OS.exit_code = 1
    elif(totals["failing"] > 0):
        OS.exit_code = 1
    if OS.exit_code == 1:
        get_tree().quit()
    else:
        call_deferred("start_integration_tests")

func start_integration_tests():
    Output.text = "Starting Integration tests..."
    call_deferred("start_ITM_server")

func start_ITM_server():
    Output.text = "Starting ITM Run Server."
    
    
