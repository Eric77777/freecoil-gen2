# Jordan's TODO List:
# Build Start new Game.
# testing further to end the game at which point I want it to be able to return 
#       to main menu, immediately after that I will add the ability to play a 
#       second pre-game.... that second pre-game will be converted to the next 
#       game mode, which will essentially be a custom mode, based on the 
#       settings the person choose. But I plan to limit the settings to be a 
#       game similar  to pre-game, but with teams as the new option.
# 2: Check if all players are back in the game.
# 4: Start to sync all of the players to reset variables. As a command to 
#       exexcute a reset function.
# 5: Wait for all players to check in that reset has finished successfully.
# 6: Start the new game mode by button push from the host.

# Known Bugs to Consider Before Alpha:
# FIXME 1. If a coordinate that is close is given in the map settings but it is
#          not exact, but within the bounds of an existing tile, the program 
#          will perfer to match on the tile. But this results in not loading 
#          any tiles to the display.
# TODO: 2. The center of any field is always the center of the downloaded tiles,
#          not the center point that is scrolled to. - NOTE TO JORDAN, THIS SHOULD BE RESOLVED.
# TODO: 3. After loading or downloading a map, you can not load another or 
#          download another with out going back and starting again. Needs more 
#          complex logic later on. - NOTE TO JORDAN, THIS SHOULD BE RESOLVED.
# FIXME: 4. The image of who you were killed by does not always match? Not sure why
#           this is even happening. Maybe the file names are different when compiled?
# TODO: Go Alpha
# TODO: Add a method to EventSync to pass the client test status to the server.
# TODO: Implement Observer:
#       1. Observer will not hit a game_state of 2, they stay at 1.
#       2. Observer Screen shows the history of events as a scrollable list.
#       3. Observer events screen can be maximized to take up all but the header.
#       4. The server will not consider an observer when determining how many 
#          players are ready to play.
# TODO: Observer client test should send results back to the server.
#        5. See if the tests can be run on a headless server now?
# TODO: Battle Royale
# FIXME: Can start the next game mode.
# TODO: Go Beta
# TODO: Standard Battle Mode
# TODO: Capture the Flag
# TODO: Integration with Digital Flag
# TODO: Go Release Canidate
# TODO: On Identify or right after, When a client rejoins, we should ask how large 
#       is their events history, if the size is different from the server, then 
#       resend them the events history to fix. Accounts for an app crash.
# Optimizations: Event Sync: 
#   1. A total of events in events history should be kept, literally an int.
#   2. sync_vars should be deleted after they are applied to save memory.
#   3. locations events should be deleted after they are applied to save memory.
#   4. the server can only delete events if all players have acknowledged a copy of the event.
#   5. many other events can be deleted if the relevant information is stored.
# TODO: Improve the shop descriptions.
# TODO: Fix bugs until good enough.
# TODO: Make a stable release!

# FIXME: The app can crash if we try to connect to a gun that was selected by 
#        another player, we need to be able to trigger this in a test, to make 
#        it happen just before you connect to the gun have some one else do it.
#       Note to Jordan:  This MAY have been resolved with a couple of checks that
#       I added to the connect script.

# Circle Back For TODO List:
# Fix Geodetic methods by validating with real world data or invalidating and 
# then fixing. These methods: get_meters_per_pixel(), get_dest_from_bearing_range()
# Observer view or better for Dedicated Host.
# Rename PregameLobby to GameSelection, because it is not really a lobby.
# TODO: Write WIFI status check for Android in Kotlin. Will need permissions.
# TODO: Write Android Battery get_percentage() in Kotlin. Will need permissions.


#Eric's Todo List:
#TODO: Improve storm syncing (base upon a timer that is continuous, that doesn't
#       pause when app is not active or is minimized).
#TODO: Go Alpha
#TODO: Determine how to layer the ScreenEffects scene beneath the Respawn, Eliminated,
#       EndofGame, etc. scenes.
#TODO: Finish implementing all items in Inventory (Playing.tscn)
#       Un-implemented items: armor_piercing_rounds, lethal_rounds
#TODO: Complete implementation of the ways that credits are obtained.
#       Simple Kills, Kills of High Profile Players, Combat Pay - Complete
#       Contracts - INCOMPLETE
#       Medic Heals - Complete
#TODO - AFTER BATTLE ROYALE IMPLEMENTATION (TEAMS USED)
    #TODO: Fully implement storms for Battle Royale.
    #TODO: Scoreboard - Add teams scoring.
    #TODO: Supply Depot - Complete Barracks (need to cue teammates and corresponding
    #       alive-status to determine what players should be added to Barracks for resurrecting).
#TODO: Go Beta
#TODO: Create IOS bluetooth plugin.
#TODO: Investigate Dedicated Host on PC using a larger screen for scenes in which
#           it would be useful (ArmorySettings, Scoreboard, etc).
#
#TODO: Periodically review all FIXME (or FIX_ME) areas in the code and resolve.
#
#FUTURE ADDITIONS:
#TODO: Loadouts:
#       Add loadouts to OfflineSettings, in Armory area.
#       Add loadouts to GameSettings (selections = "Random Weapons", "Loadouts").
#       Add loadouts to lobbies (user defined/selected or host defined/selected?).
#TODO: AssignWeapon Scene:
#       Add changing of weapon name after connection to weapon.
#       Add transfer of all weapon names from Host to other players.
#TODO: Standard Header
#       WIFI (Wifi Signal) - Short-term this will be repurposed to register connection to a game
#       MOBILE (Mobile Battery) - No native Mobile Battery support (get_power_percent_left)
#                                   for Android.  Wrote script for MacOS and Windows
#                                   support.  Need additional plugin?
#       GPS (Signal), BLUETOOTH (Connection), BATTERY (Weapon Battery) - COMPLETE
#TODO: FreecoiLInterface.gd, plugin, etc.
#       Add multiple Laser ID capability to app (ask Jordan for details of what's needed).
#       Add game setting of requiring plugged-in IR receiver.  Add Clip-On RX Check Interval setting
#       in FreecoiLInterface.gd and plugin.  Apply damage to player when IR receiver is unplugged.
#TODO: Add flame thrower and other special weapons to armory (etc).
#TODO: Add a notification for hits (who was hit...TBD if this is too much for the network).
#
#
#FOR REFERENCE:
#   STANDARD VARIABLE NAMING: All lower case, words separated by underscore
#       (Example: variable_name)
#   See Template.gd for reference and explanation (comments) on State Class
#       implementation on scenes
#   For Debugging of MainScreen Scene/Screens
#       Splash.gd: Change State Class names in elif-statement of
#           background_load_state_ahead (Lines 258 & 375) 
#       Settings.gd: Add/Edit correct variable in experimental_toggles to "true" (Line 34)
#   For Show/Hide of the StandardHeader, toggle the
#       PM.get_tree().root.get_node("Container/UI").get_child(0).visible OR
#       PM.get_tree().root.get_node("Container/UI/StandardHeader").visible
#       in the on_enter and on_exit of the State Class.
#   GUT Testing
#       To turn testing off, open the scene Logic/EverGreen/ProgramManager.tscn then select the
#       ProgramManager Node. On the right uncheck the Run_ITM and you can uncheck clean_user_dir.
#   Sync Variable Commands (Examples below):
#       FOR SERVER PASSING VALUES TO CLIENTS
#           All args are passed in an array, in the order that you expect your method to unpack them.
#           A good example is GameCommon.gd Line 1207:
#           func remote_set_player_laser_id(args)
#               PM.Networking.EventSync.record_event("server_request", {"mup": "all", 
#                       "method": "set_player_start_credits", "object": "GameCommon", "args": [2000]}
#                       , PM.Networking.get_server_time_usec())
#       FOR CLIENT PASSING VALUES TO SERVER
#           All arguments are passed in an array.
#           A Good example is GameCommon.gd Line 939 
#           func notify_game_state_change(mup, args)
#               PM.Networking.EventSync.record_event("client_request", 
#                       {"method": "notify_game_state_change", "object": "GameCommon",
#                       "args": [2]}, PM.Networking.get_server_time_usec())
