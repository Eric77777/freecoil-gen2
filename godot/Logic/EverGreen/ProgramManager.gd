extends Node2D

const GODOT_MAX_INT: int = 9223372036854775807

var operating_system: String = ""
var unique_id: String = ""
var max_threads: int = 1
var display_metrics: Dictionary = {}

var jump_to_state_post_splash: String = ""
var checked_ITM_once: bool = false
var run_Master_Test_Manager: bool = false
var run_ITM: bool = false
var activate_ITM_Player: bool = false
var ITM_act_as_server: bool = false
var fake_unique_id: String = ""
var fake_os: String = ""
var ITM_allow_launch_clients: bool = false
var activate_ITM_Recorder: bool = false
var ITM_test_path: String = ""

var erase_user_dir:bool = false
var ran_erase_user_data_dir: bool = false
var artificial_delay_between_states: float = 0



var player_type: String = ""
var player_team: int = -1

var checked_for_MTM_once: bool = false
var checked_for_GUT_once: bool = false
var run_GUT_unit_tests: bool = false
var run_specified_GUT_test: bool = false
var checked_for_specified_GUT_once: bool = false
var load_gut_post_splash: bool = false
var checked_each_once: bool = false

var States: Dictionary = {}
var Settings: Resource
var Helpers: Resource
var SS: Dictionary = {}  # SuperSystems Name Space
var TheContainer: Node

var Networking: Node
var BackAndEscape: Label
var LoadingScreen: ReferenceRect
var IntegrationTestManager: Node
var itm_thread_num:int 
var mtm_thread_num: int
var InitialLoadingOutput: Label
var InitialLoadingSpinner
var current_state: String = ""
var current_state_ref: Node
var previous_state: String = ""
var the_state_stack: Array = []
var screenshot_finished: bool = false
var visual_loading_stopped_for_screenshot: bool = false
var loading_thread_num_0: int
var loading_thread_num_1: int
var loading_thread_num_2: int
var loading_thread_num_3: int
var loading_thread_num_4: int
var loading_thread_num_5: int
var loading_thread_num_6: int
var loading_thread_num_7: int
var theme_loading_thread_num: int 
var main_menu_empty_is_ready: bool = false

var splash_screen_loaded: bool = false
var submodules_ready: bool = false
var helpers_loaded: bool = false
var back_request_counter: int = 0
var continue_spinner_updates: bool = true
var theme_loaded: bool = false
var command_line_args: Dictionary = {}
var parsed_command_line_args_once: bool = false

func _ready():
    TheContainer = get_tree().root.get_node_or_null("Container")
    if TheContainer == null:
        pass
    else:
        #Backup/Copy the variables across
        jump_to_state_post_splash = TheContainer.jump_to_state_post_splash
        run_ITM = TheContainer.run_ITM
        run_Master_Test_Manager = TheContainer.run_Master_Test_Manager
        activate_ITM_Player = TheContainer.activate_ITM_Player
        ITM_act_as_server = TheContainer.ITM_act_as_server
        fake_unique_id = TheContainer.fake_unique_id
        fake_os = TheContainer.fake_os
        ITM_allow_launch_clients = TheContainer.ITM_allow_launch_clients
        activate_ITM_Recorder = TheContainer.activate_ITM_Recorder
        ITM_test_path = TheContainer.ITM_test_path
        erase_user_dir = TheContainer.erase_user_dir
        run_GUT_unit_tests = TheContainer.run_GUT_unit_tests
        run_specified_GUT_test = TheContainer.run_specified_GUT_test
        artificial_delay_between_states = TheContainer.artificial_delay_between_states
        player_type = TheContainer.player_type
        player_team = TheContainer.player_team
    # Sanity check our variables
    var bad_configuration = false
    if run_GUT_unit_tests:
        if run_ITM:
            bad_configuration = true
            printerr("PM: Configuration Error: You can not both run GUT and " +
                "ITM at the same time."
            )
        if run_specified_GUT_test:
            bad_configuration = true
            printerr("PM: Configuration Error: You can not both run GUT and " +
                "specified GUT at the same time."
            )
    elif run_specified_GUT_test:
        if run_ITM:
            bad_configuration = true
            printerr("PM: Configuration Error: You can not both run specified" +
                " GUT and ITM at the same time."
            )
    elif run_ITM:
        if activate_ITM_Player and activate_ITM_Recorder:
            bad_configuration = true
            printerr("PM: Configuration Error: You can not activate ITM Player" +
                " and ITM Recorder at the same time."
            )
        elif not activate_ITM_Player and not activate_ITM_Recorder:
            bad_configuration = true
            printerr("PM: Configuration Error: You MUST activate either ITM " +
                "Player or ITM Recorder if run_ITM is true."
            )
    if bad_configuration:
        get_tree().quit(31)  # ERR_INVALID_PARAMETER = 31 --- Invalid parameter error.
    BackAndEscape = get_tree().root.get_node_or_null("Container/Camera/TopLayer" +
        "/BackAndEscapeWarning"
    )

func _draw():
    if not splash_screen_loaded:
        splash_screen_loaded = true
        call_deferred("load_and_setup_Helpers")
        call_deferred("load_and_setup_Settings")
        call_deferred("detect_vital_data")

func evaluate_state_change(next_state):
    if next_state == "#GoBack#":
        the_state_stack.pop_back()
        if the_state_stack.size() > 0:
            next_state = the_state_stack[-1]
    elif next_state == "#GUT_ALL#":
        next_state = "GutAllUnit"
    elif load_gut_post_splash:
        load_gut_post_splash = false
        next_state = "GutAllUnit"
    elif jump_to_state_post_splash != "":
        next_state = jump_to_state_post_splash
        jump_to_state_post_splash = ""
    elif run_Master_Test_Manager:
        next_state = "MasterTestManager"
    elif next_state != current_state:
        if next_state in States:
            the_state_stack.append(next_state)
        else:
            PM.Settings.Log("State not found: " + str(next_state), "critical")
            next_state = current_state
    if next_state != current_state:
        if current_state != "":
            get_node(current_state).on_exit()
        previous_state = current_state
        Settings.Session.set_data("ProgramManager_state_trigger", null)
        current_state = next_state
        PM.LoadingScreen.fade_in()
        LoadingScreen.progress(1.0)
        LoadingScreen.item_name(current_state)
        loading_thread_num_0 = Helpers.background_loader_n_callback(
                States[current_state], self, "finish_state_change")

func finish_state_change(background_results):
    var results = background_results[loading_thread_num_0]
    if results == "finished":
        var loaded_scene = Helpers.get_resource_from_background_n_disconnect(
                loading_thread_num_0, self, "finish_state_change").instance()
        if artificial_delay_between_states != 0:
            yield(get_tree().create_timer(artificial_delay_between_states), "timeout")
        add_child(loaded_scene)
        current_state_ref = loaded_scene
        if current_state_ref.name == "Example":
            printerr("PM: Error: The State ID of the ProgramManagerState has not been changed " +
                "from #Example# to something else! It can not be '#Example#."
            )
        get_node(current_state).on_enter(previous_state)

func _process(_delta) -> void:
    if not parsed_command_line_args_once:
        parsed_command_line_args_once = true
        parse_command_line_args()
    if not checked_for_GUT_once:
        checked_for_GUT_once = true
        if run_GUT_unit_tests:
            load_gut_in_container_post_splash()  
    if not checked_for_specified_GUT_once:
        checked_for_specified_GUT_once = true
        if run_specified_GUT_test: 
            run_specified_tests()
    if not checked_for_MTM_once:
        checked_for_MTM_once = true
        if run_Master_Test_Manager:
            load_MTM()
    if not checked_ITM_once:
        checked_ITM_once = true
        if run_ITM:
            load_ITM()
    if not ran_erase_user_data_dir:
        ran_erase_user_data_dir = true
        if erase_user_dir or run_ITM:
            erase_user_data_dir()
    if not checked_each_once:
        checked_each_once = true
        if run_specified_GUT_test:
            pass
        elif run_GUT_unit_tests:
            pass
        elif run_ITM:
            pass
        elif run_Master_Test_Manager:
            pass
        else:
            call_deferred("defered_loading")

        

func load_and_setup_Helpers():
    Helpers = load("res://Logic/EverGreen/Helpers.gd").new()
    helpers_loaded = true

func load_and_setup_Settings():
    Settings = load("res://Logic/EverGreen/Settings.gd").new()
    while not helpers_loaded:
        yield(get_tree(), "idle_frame")
    submodules_ready = true
    
  
func call_next_frame(obj, method):
    yield(get_tree(),"idle_frame")
    obj.call_deferred(method)

################################################################################
# Application Lifecycle Functions
func goto_main_menu():
    if current_state != "MainMenu":
        call_deferred("evaluate_state_change",  "MainMenu")

func _notification(what):
    if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
        get_tree().quit() # default behavior
    if what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST:
        handle_back_request()
    if what == MainLoop.NOTIFICATION_APP_RESUMED:
        pass
#        if rect_position.x == x_ingame:
#            if get_tree().paused == false:  # We want player to press resume, when ready.
#                get_tree().paused = true
#            rect_position.x = x_pause_menu
#        else:
#            if get_tree().paused == false:
#                get_tree().paused = true
    if what == MainLoop.NOTIFICATION_APP_PAUSED:
        pass
#        if rect_position.x == x_ingame:
#            rect_position.x = x_pause_menu
#            get_tree().call_group("AppControl", "ac_app_pause_now")
#            yield(get_tree(), "idle_frame")
#            while count_paused != count_needed_to_pause:
#                yield(get_tree(), "idle_frame")
#            if get_tree().paused == false:
#                get_tree().paused = true
#        else:
#            rect_position.x = x_pause_menu
#            get_tree().call_group("AppControl", "ac_app_pause_now")
#            yield(get_tree(), "idle_frame")
#            if get_tree().paused == false:
#                get_tree().paused = true

func handle_back_request():
    pass
#    if current_state != "MainMenu":
#        call_deferred("evaluate_state_change",  "MainMenu")
        
func handle_back_and_escape_requests_the_same():
#    if rect_position.x == x_ingame:
#        if get_tree().paused == false:
#            get_tree().paused = true
#            rect_position.x = x_pause_menu
#    elif rect_position.x == x_pause_menu:
#        get_tree().call_group("AppControl", "ac_end_game_immediately")
#        get_tree().paused = false
#    elif rect_position.x != x_main_menu:
#        go_to_mainmenu()
#        if get_tree().paused == true:
#            get_tree().paused = false
#    else:  # We are on the main menu and you pressed back.
#        if back_request_counter == 1:  # if you press back twice then we exit.
#            get_tree().quit() # default behavior
#        else:
#            back_request_counter += 1
        show_back_and_escape_warning()

func show_back_and_escape_warning():
    BackAndEscape.show()
    yield(get_tree().create_timer(4), "timeout")
    BackAndEscape.hide()
    back_request_counter = 0

func global_pause():
    Settings.Session.set_data("APP_paused", true)
    get_tree().set_pause(true)

func global_unpause():
    Settings.Session.set_data("APP_paused", false)
    get_tree().set_pause(false)

# End Application Lifecycle Functions
################################################################################

# Splash & Initial Start Up Loading
################################################################################
func detect_vital_data():
    if OS.get_processor_count() - 1 < 1:
        max_threads = 1
    else:
        max_threads = OS.get_processor_count() - 1
    if fake_os == "":
        operating_system = OS.get_name()
    else:
        operating_system = fake_os
    if fake_unique_id == "":
        unique_id = OS.get_unique_id()
    else:
        unique_id = fake_unique_id
    while not helpers_loaded:
        yield(get_tree(), "idle_frame")
    display_metrics = Helpers.get_display_metrics()

func defered_loading():
    InitialLoadingSpinner = get_tree().root.get_node(
            "Container/Scene0/Splash/CenterContainer/VBoxContainer/LoadingSpinner")
    InitialLoadingOutput = get_tree().root.get_node(
            "Container/Scene0/Splash/CenterContainer/VBoxContainer/LoadingLabel")
    InitialLoadingOutput.text = "Initializing: Settings..."
    Settings.initialize_settings()
    Settings.Session.set_data("APP_paused", false)
    Settings.Session.set_data("APP_exiting", false)
    Settings.Session.set_data("APP_go_back", 0)  # Is just a counter.
    Settings.Session.set_data("APP_resuming", false)
    Settings.InGame.set_data("except_pause_disable_input", false)
    Settings.Session.set_data("current_menu", "0,0")
    Settings.Session.set_data("previous_menu", "0,0")
    get_tree().call_group("Camera", "delayed_setup")
    InitialLoadingOutput.text = "Loading: Animated Loading Spinner..."
    loading_thread_num_1 = PM.Helpers.background_loader_n_callback(
            "res://assets/images/SpriteSheets/FeralInfinitySpriteSheet.tres", 
            self, "setup_loading_spinner")
    InitialLoadingOutput.text = "Loading: FeralBytes Logo..."
    loading_thread_num_2 = PM.Helpers.background_loader_n_callback(
            "res://assets/images/logos/FeralBytes_logo.png", 
            self, "setup_feralbytes_logo")
    call_deferred("background_load_theme")
    call_deferred("load_game_logo")
    if Settings.DEBUG_LEVEL > 0:
        screenshot_finished = true
    call_deferred("populate_state_names")
    call_deferred("background_load_loading_scene")
    call_deferred("background_load_networking")
   
func setup_loading_spinner(background_results):
    var results = background_results[loading_thread_num_1]
    if results == "finished":
        if not visual_loading_stopped_for_screenshot:
            if is_instance_valid(InitialLoadingOutput):
                InitialLoadingOutput.text = "Setting Up: Animated Loading Spinner..."
        if is_instance_valid(InitialLoadingSpinner):
            InitialLoadingSpinner.frames = PM.Helpers.get_resource_from_background_n_disconnect(
                    loading_thread_num_1,self, "setup_loading_spinner")
        yield(get_tree(), "idle_frame")
        if is_instance_valid(InitialLoadingSpinner):
            InitialLoadingSpinner.frame = 0
            InitialLoadingSpinner.play("default")
            call_deferred("manually_advance_spinner")

func manually_advance_spinner():
    var last_manual_frame = 0
    yield(get_tree().create_timer(0.1), "timeout")
    while continue_spinner_updates:
        if is_instance_valid(InitialLoadingSpinner):
            if InitialLoadingSpinner.frame != last_manual_frame:
                continue_spinner_updates = false
                break
            if InitialLoadingSpinner.frame == 3:
                InitialLoadingSpinner.frame = 0
            else:
                InitialLoadingSpinner.frame += 1
            last_manual_frame = InitialLoadingSpinner.frame
            yield(get_tree().create_timer(0.2), "timeout")
        else:
            continue_spinner_updates = false
            break

func setup_feralbytes_logo(background_results):
    var results = background_results[loading_thread_num_2]
    if results == "finished":
        if InitialLoadingOutput != null:
            if not visual_loading_stopped_for_screenshot:
                if is_instance_valid(InitialLoadingOutput):
                    InitialLoadingOutput.text = "Setting Up: FeralBytes Logo..."
        else:
            LoadingScreen.item_name("FeralBytes Logo")
        var feralbytes_logo_ref = get_tree().root.get_node(
                "Container/Scene0/Splash/CenterContainer/VBoxContainer/NinePatchRect2")
        if is_instance_valid(feralbytes_logo_ref):
            feralbytes_logo_ref.texture = PM.Helpers.get_resource_from_background_n_disconnect(
                    loading_thread_num_2, self, "setup_feralbytes_logo")
        if Settings.DEBUG_LEVEL == 0:
            pass  # Not transparent and no fade in.
        else:
            feralbytes_logo_ref.modulate = Color(1,1,1,0)  # Transparent
            call_deferred("fade_in_logo", feralbytes_logo_ref)
        #call_deferred("load_game_logo")

func fade_in_logo(logo_ref):
    var visibility = logo_ref.modulate
    while visibility.a < 1.0:
        visibility.a = visibility.a + 0.01
        if is_instance_valid(logo_ref):
            logo_ref.modulate = visibility
            yield(get_tree(), "idle_frame")
        else:
            break
    
func load_game_logo():
    if not visual_loading_stopped_for_screenshot:
        InitialLoadingOutput.text = "Loading: Logo..."
    loading_thread_num_3 = PM.Helpers.background_loader_n_callback("res://assets/images/logos/" + 
            "Logo.png", 
            self, "setup_game_logo")
    
func setup_game_logo(background_results):
    var results = background_results[loading_thread_num_3]
    if results == "finished":
        if is_instance_valid(InitialLoadingOutput):
            InitialLoadingOutput.text = "Setting Up: Logo..."
        if get_tree().root.get_node("Container/Scene0/").has_node("Splash"):
            var game_logo_ref = get_tree().root.get_node(
                "Container/Scene0/Splash/CenterContainer/VBoxContainer/NinePatchRect"
            )
            if is_instance_valid(game_logo_ref):
                game_logo_ref.texture = PM.Helpers.get_resource_from_background_n_disconnect(
                        loading_thread_num_3, self, "setup_game_logo")
        if Settings.DEBUG_LEVEL == 0:  # Do the screenshot for boot_splash.png
            # Not transparent and no fade in.
            var color_rect_ref = get_tree().root.get_node(
                "Container/Scene0/Splash/CenterContainer/VBoxContainer/ColorRect"
            )
            color_rect_ref.visible = false
            var spinner_ref = get_tree().root.get_node(
                "Container/Scene0/Splash/CenterContainer/VBoxContainer/LoadingSpinner"
            )
            spinner_ref.visible = false
            var signals_used_ref = get_tree().root.get_node(
                "Container/Camera/TopLayer/SignalsUsed"
            )
            signals_used_ref.visible = false
            var debug_output_ref = get_tree().root.get_node(
                "Container/Camera/TopLayer/DebugOutput"
            )
            debug_output_ref.visible = false
            visual_loading_stopped_for_screenshot = true
            yield(get_tree(), "idle_frame")
            InitialLoadingOutput.text = ""
            var clear_mode = get_viewport().get_clear_mode()
            get_viewport().set_clear_mode(Viewport.CLEAR_MODE_ONLY_NEXT_FRAME)
            # Wait until the frame has finished before getting the texture.
            yield(VisualServer, "frame_post_draw")
            # Retrieve the captured image.
            var img = get_viewport().get_texture().get_data()
            get_viewport().set_clear_mode(clear_mode)
            # Flip it on the y-axis (because it's flipped).
            img.flip_y()
            img.save_png("res://assets/images/boot_splash.png")
            color_rect_ref.visible = true
            spinner_ref.visible = true
            signals_used_ref.visible = true
            debug_output_ref.visible = true
            screenshot_finished = true
            visual_loading_stopped_for_screenshot = false
        else:
            if get_tree().root.get_node("Container/Scene0/").has_node("Splash"):
                var game_logo_ref = get_tree().root.get_node(
                    "Container/Scene0/Splash/CenterContainer/VBoxContainer/NinePatchRect"
                )
                if is_instance_valid(game_logo_ref):
                    game_logo_ref.modulate = Color(1,1,1,0)  # Transparent
                    call_deferred("fade_in_logo", game_logo_ref)

func populate_state_names():
    if not visual_loading_stopped_for_screenshot:
        if InitialLoadingOutput != null:
            if is_instance_valid(InitialLoadingOutput):
                InitialLoadingOutput.text = "Loading: Populating states..."
    Settings.Log("Program Manager: Populating states...")
    var dir = Directory.new()
    dir.open("res://Logic/ProgramManagerStates/")
    dir.list_dir_begin()
    while true:
        var file = dir.get_next()
        if file == "":
            break
        elif not file.begins_with("."):
            # https://github.com/godotengine/godot/issues/42507
            if file.matchn("*.tscn"):
                var file_path = "res://Logic/ProgramManagerStates/" + file
                file = file.replace(".tscn", "")
                if not visual_loading_stopped_for_screenshot:
                    if InitialLoadingOutput != null:
                        if is_instance_valid(InitialLoadingOutput):
                            InitialLoadingOutput.text = ("Populated: Program " +
                                "Manager Main Menu State - " + file + "..."
                            )
                States[file] = file_path
                Settings.Log("Program Manager: Populated: " + file, "testing")
    dir.list_dir_end()
    Settings.Log("Program Manager: Main Menu State Loading Completed.")
    
func background_load_networking():
    InitialLoadingOutput.text = "Loading: Networking..."
    loading_thread_num_4 = PM.Helpers.background_loader_n_callback("res://Logic" +
        "/EverGreen/Networking/Networking.gd", self, "setup_networking"
    )

func setup_networking(background_results):
    var results = background_results[loading_thread_num_4]
    if results == "finished":
        if is_instance_valid(InitialLoadingOutput):
            InitialLoadingOutput.text = "Setting Up: Networking..."
        else:
            if LoadingScreen != null:
                LoadingScreen.item_name("Setting Up: Networking...")
        var temp = PM.Helpers.get_resource_from_background_n_disconnect(
               loading_thread_num_4,self, "setup_networking")
        PM.add_child(temp.new())
        PM.Networking = PM.get_node("Networking")
        call_deferred("background_load_main_menu_empty")
    
func background_load_main_menu_empty():
    if not visual_loading_stopped_for_screenshot:
        if InitialLoadingOutput != null:
            if is_instance_valid(InitialLoadingOutput):
                InitialLoadingOutput.text = "Loading: Empty Main Scene..."
    loading_thread_num_5 = PM.Helpers.background_loader_n_callback("res://scenes/Global/Empty.tscn", 
            self, "setup_main_menu_empty")

func setup_main_menu_empty(background_results):
    var results = background_results[loading_thread_num_5]
    if results == "finished":
        if not visual_loading_stopped_for_screenshot:
            if InitialLoadingOutput != null:
                if is_instance_valid(InitialLoadingOutput):
                    InitialLoadingOutput.text = "Setting Up: Empty Main Scene..."
        var temp = PM.Helpers.get_resource_from_background_n_disconnect(
                    loading_thread_num_5, self, "setup_main_menu_empty")
        while not get_tree().root.get_node("Container").initialized:
            yield(get_tree(), "idle_frame")
        while not screenshot_finished:
            yield(get_tree(), "idle_frame")
        while LoadingScreen == null:
            yield(get_tree(), "idle_frame")
        get_tree().call_group("Container", "set_new_scene", temp)
        yield(get_tree(), "idle_frame")
        main_menu_empty_is_ready = true
        call_deferred("load_next_state")
        
func background_load_theme():
    if not visual_loading_stopped_for_screenshot:
        InitialLoadingOutput.text = "Loading: Theme..."
    theme_loading_thread_num = Helpers.background_loader_n_callback(
            "res://assets/themes/default.tres", self, "setup_theme")

func setup_theme(background_results):
    var results = background_results[theme_loading_thread_num]
    if results == "finished":
        if is_instance_valid(InitialLoadingOutput):
            if not visual_loading_stopped_for_screenshot:
                InitialLoadingOutput.text = "Setting Up: Theme..."
        var temp = Helpers.get_resource_from_background_n_disconnect(theme_loading_thread_num,
                self, "setup_theme")
        theme_loaded = true
        while not main_menu_empty_is_ready:
            yield(get_tree(), "idle_frame")
        get_tree().root.get_node("Container").current_scene.theme = temp

func background_load_loading_scene():
    while not theme_loaded:
        yield(get_tree(), "idle_frame")
    if is_instance_valid(InitialLoadingOutput):
        if not visual_loading_stopped_for_screenshot:
            InitialLoadingOutput.text = "Loading: The Loading Scene..."
    loading_thread_num_6 = PM.Helpers.background_loader_n_callback("res://scenes/Global/Loading.tscn", 
            self, "setup_loading_scene")

func setup_loading_scene(background_results):
    var results = background_results[loading_thread_num_6]
    if results == "finished":
        if is_instance_valid(InitialLoadingOutput):
            if not visual_loading_stopped_for_screenshot:
                InitialLoadingOutput.text = "Setting Up: The Loading Scene..."
        var temp = PM.Helpers.get_resource_from_background_n_disconnect(
                loading_thread_num_6, self, "setup_loading_scene").instance()
        get_tree().root.get_node("Container/Loading").add_child(temp)
        temp.visible = false
        temp.modulate.a = 0
        LoadingScreen = get_tree().root.get_node("Container/Loading/Loading")
        
func load_next_state():
    Settings.Log("Program Manager: Loading Next state...")
    while not main_menu_empty_is_ready:
        yield(get_tree(), "idle_frame")
    while LoadingScreen == null:
        yield(get_tree(), "idle_frame")
    
    LoadingScreen.fade_in()
    LoadingScreen.progress(1.0)
    LoadingScreen.item_name("Loading: Post Splash State...")
    evaluate_state_change("PostSplash")
#    current_state = "PostSplash"
#    the_state_stack.append(current_state)
#    loading_thread_num_7 = Helpers.background_loader_n_callback(
#                States[current_state], self, "setup_next_state")
#
#func setup_next_state(background_results): 
#    var results = background_results[loading_thread_num_7]
#    if results == "finished":
#        var loaded_scene = Helpers.get_resource_from_background_n_disconnect(
#                loading_thread_num_7, self, "setup_next_state").instance()
#        add_child(loaded_scene)
#        get_node(current_state).on_enter(null)

func load_MTM():
    splash_screen_loaded = true
    while not submodules_ready:
        yield(get_tree(), "idle_frame")
    mtm_thread_num = PM.Helpers.background_loader_n_callback(
        "res://tests/TestingHelpers/TestManager/MTM/MTM.tscn", self, "setup_MTM"
    )
        
func setup_MTM(background_results):
    var results = background_results[mtm_thread_num]
    if results == "finished":
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                mtm_thread_num, self, "setup_MTM").instance()
        get_tree().root.add_child(loaded_scene)
        call_deferred("defered_loading")

func load_ITM():
    splash_screen_loaded = true
    while not submodules_ready:
        yield(get_tree(), "idle_frame")
    var ITM_path
    if activate_ITM_Player:
        ITM_path  = "res://tests/TestingHelpers/TestManager/ITM_Player/ITM_Player.tscn"
    else:
        ITM_path = "res://tests/TestingHelpers/TestManager/ITM_Recorder/ITM_Recorder.tscn"
    itm_thread_num = PM.Helpers.background_loader_n_callback(
        ITM_path, self, "setup_itm"
    )

func setup_itm(background_results):
    var results = background_results[itm_thread_num]
    if results == "finished":
        var loaded_scene = PM.Helpers.get_resource_from_background_n_disconnect(
                itm_thread_num, self, "setup_itm").instance()
        get_tree().root.add_child(loaded_scene)
        call_deferred("defered_loading")

func parse_command_line_args():
    var arguments = {}
    for argument in OS.get_cmdline_args():
        # Parse valid command-line arguments into a dictionary
        if argument.find("=") > -1:
            var key_value = argument.split("=")
            arguments[key_value[0].lstrip("--")] = key_value[1]
    command_line_args = arguments
    command_line_set_vars(arguments)
    return arguments

func command_line_set_vars(args):
    if "run_Master_Test_Manager" in args:
        if bool(args["run_Master_Test_Manager"]):
            run_Master_Test_Manager = true
        else:
            run_Master_Test_Manager = false
    if "run_ITM" in args:
        if bool(args["run_ITM"]):  # is true
            run_ITM = true
        else:
            run_ITM = false
    if "activate_ITM_Player" in args:
        if bool(args["activate_ITM_Player"]):
            activate_ITM_Player = true
        else:
            activate_ITM_Player = false
    if "ITM_act_as_server" in args:
        if bool(args["ITM_act_as_server"]):
            ITM_act_as_server = true
        else:
             ITM_act_as_server = false
    if "fake_unique_id" in args:
        fake_unique_id = args["fake_unique_id"]
    if "ITM_allow_launch_clients" in args:
        if bool(args["ITM_allow_launch_clients"]):
            ITM_allow_launch_clients = true
        else:
            ITM_allow_launch_clients = false
    if "activate_ITM_Recorder" in args:
        if bool(args["activate_ITM_Recorder"]):
            activate_ITM_Recorder = true
        else:
            activate_ITM_Recorder = false
    if "ITM_test_path" in args:
        ITM_test_path = args["ITM_test_path"]
    if erase_user_dir in args:
        if bool(args["erase_user_dir"]):
            erase_user_dir = true
        else:
            erase_user_dir = false
    if "run_GUT_unit_tests" in args:
        if bool(args["run_GUT_unit_tests"]):
            run_GUT_unit_tests = true
        else:
             run_GUT_unit_tests = false
    if "run_specified_GUT_test" in args:
        if bool(args["run_specified_GUT_test"]):
            run_specified_GUT_test = true
        else:
            run_specified_GUT_test = false
    if "player_type" in args:
        player_type = args["player_type"]
    if "player_team" in args:
        player_team = int(args["player_team"])
    
    
func erase_user_data_dir():
    while not submodules_ready:
        yield(get_tree(), "idle_frame")
    var returned  = PM.Helpers.get_dir_contents("user://")
    for file_path in returned[0]:
        var dir = Directory.new()
        dir.open(file_path)
        dir.remove(file_path)

func load_gut_only():
    var __ = get_tree().change_scene("res://tests/TestRunner.tscn")
#    var gut_loaded = false
#    while not gut_loaded:
#        yield(get_tree(), "idle_frame")
#        if get_tree().root.has_node("Gut"):
#            pass
#            if get_tree().root.get_node("Gut")._gut != null:
#                gut_loaded = true
    # Adjusting the size of GUT using scaling does not work well unfortunately.

func load_gut_in_container_post_splash():
    load_gut_post_splash = true
    call_deferred("defered_loading")

func run_specified_tests():
    splash_screen_loaded = true
    # warning-ignore:RETURN_VALUE_DISCARDED
    get_tree().change_scene("res://tests/SpecifiedTestRunner.tscn")
    var gut_loaded = false
    while not gut_loaded:
        yield(get_tree(), "idle_frame")
        if get_tree().root.has_node("Gut"):
            if get_tree().root.get_node("Gut")._gut != null:
                gut_loaded = true
    #get_tree().root.get_node("Gut").get_gut().get_gui().rect_scale.x = 0.73

func a_button_was_pressed(node):
    if run_ITM and activate_ITM_Recorder:
        var recorder = get_tree().root.get_node_or_null("/root/ITM_Recorder")
        var new_event = {"type": "ButtonPressed", "node_path": node.get_path()}
        recorder._input(new_event)
