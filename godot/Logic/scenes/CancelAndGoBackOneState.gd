extends Button

func _on_Cancel_pressed():
    disabled = true
    PM.Settings.Session.set_data("ProgramManager_state_trigger", false)
