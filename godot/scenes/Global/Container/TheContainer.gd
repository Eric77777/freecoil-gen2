extends Node

export(bool) var Program_Manager: bool = true
export(String, FILE, ".tscn") var jump_to_state_post_splash: String = ""
export(bool) var run_Master_Test_Manager: bool = false
export(bool) var run_ITM: bool = false
export(bool) var activate_ITM_Player: bool = false
export(bool) var ITM_act_as_server: bool = false
export(String, "", "930072000cf0009c8b200c608ef005da", "930072000cf0009c8b200c608ef005db",
    "930072000cf0009c8b200c608ef005dc", "930072000cf0009c8b200c608ef005dd"
) var fake_unique_id: String = ""
export(String, "", "Android") var fake_os: String = ""
export(bool) var ITM_allow_launch_clients: bool = false
# NOTE: To exit recording press F6
export(bool) var activate_ITM_Recorder: bool = false
export(String, DIR) var ITM_test_path: String = ""

export(bool) var erase_user_dir: bool = false

export(bool) var run_GUT_unit_tests: bool = false
export(bool) var run_specified_GUT_test: bool = false
export(float) var artificial_delay_between_states: float = 0

export(String) var player_type: String = ""
export(int) var player_team: int = -1

# Declare member variables here.
var current_scene
var previous_scene
var active_scene_container = 0
#warning-ignore:unused_class_variable
var scene_loaded = false
var threaded_scene_loader1 = null
var threaded_scene_loader2 = null
var loading_state2 = "idle"
var integration_testing = false
var SceneFader
var ui_child_offset: Vector2
var after_5_frames = 0
var initialized = false

var SuperSystems: Dictionary = {}
var supersystem_execution_order: Array = []

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    if PM.submodules_ready:
        if after_5_frames >= 5:
            if not initialized:
                PM.Settings.Session.set_data("Container_loading_state", "idle")
                add_to_group("Container")
                current_scene = $Scene0.get_child(0)
                initialized = true
            else:
                if len(supersystem_execution_order) > 0:
                    for system in supersystem_execution_order:
                        if SuperSystems[system].has_method("on_process"):
                            SuperSystems[system].on_process(delta)
        else:
            after_5_frames += 1
            
func _physics_process(delta):
    if len(supersystem_execution_order) > 0:
        for system in supersystem_execution_order:
            if SuperSystems[system].has_method("on_physiscs_process"):
                SuperSystems[system].on_physics_process(delta)

func _input(event):
    if len(supersystem_execution_order) > 0:
        for system in supersystem_execution_order:
            if SuperSystems[system].has_method("on_input"):
                SuperSystems[system].on_input(event)
                
func _unhandled_input(event):
    if len(supersystem_execution_order) > 0:
        for system in supersystem_execution_order:
            if SuperSystems[system].has_method("on_unhandled_input"):
                SuperSystems[system].on_unhandled_input(event)

func do_remote_goto_scene(path):
    PM.Settings.Log("This method is deprecated. Method: do_remote_goto_scene()", "critical")
    rpc("remote_goto_scene", path)

remotesync func remote_goto_scene_old(path):
    PM.Settings.Log("This method is deprecated. Method: remote_goto_scene()", "critical")
    if get_tree().get_rpc_sender_id() == 1:
        get_tree().call_group("MenuButtons", "enabled", false)
        get_tree().call_group("Camera", "pan_camera", 0, 0)
        goto_scene_old(path)

func goto_scene_old(scene_path):
    PM.Settings.Log("This method is deprecated. Method: goto_scene()", "critical")
    while PM.Settings.Session.get_data("Container_loading_state") != "idle":  
        yield(get_tree(), "idle_frame")
    PM.Settings.Log("Going to scene: " + str(scene_path))
    background_load_scene_old(scene_path)
    if SceneFader != null:
        get_tree().call_group("SceneFader", "fade_in")

func background_load_scene_old(scene_path,  resource_type="scene"):
    PM.Settings.Log("This method is deprecated. Method: background_load_scene()", "critical")
    if resource_type == "scene":
        PM.Settings.Log("Background loading scene: " + str(scene_path))
        if PM.Settings.Session.get_data("Container_loading_state") != "idle":
            return ERR_ALREADY_IN_USE
        else:
            PM.Settings.Session.set_data("Container_loading_state", "loading")
        # start your "loading..." animation
        threaded_scene_loader1 = Thread.new()
        threaded_scene_loader1.start(self, '_threaded_loading', [scene_path])
    elif resource_type == "scene_fader":
        threaded_loading2([scene_path, "scene_fader"])
    elif resource_type == "background":
        threaded_loading2([scene_path, "background"])
            
func threaded_loading2(user_data):
    while loading_state2 != "idle":
        yield(get_tree(), "idle_frame")
    loading_state2 = "loading"
    threaded_scene_loader2 = Thread.new()
    threaded_scene_loader2.start(self, '_threaded_loading', user_data)
    
func _threaded_loading_old(user_data):
    PM.Settings.Log("This method is deprecated. Method: _threaded_loading", "critical")
    var path = null
    var resource_type = "scene"
    for i in range(0, len(user_data)):
        match i:
            0:
                path = user_data[0]
            1:
                resource_type = user_data[1]
    var progress = 0.0
    var loader = ResourceLoader.load_interactive(path)
    var err = OK
    call_deferred('update_progress', [progress])
    while err == OK:  # ERR_FILE_EOF = loading finished
        err = loader.poll()
        if err == OK:
            progress = float(loader.get_stage()) / loader.get_stage_count()
        else:
            progress = 0.99
        call_deferred('update_progress', [progress])
    if err == ERR_FILE_EOF:
        var resource = loader.get_resource()
        match resource_type:
            "scene":
                call_deferred('set_new_scene')
            "scene_fader":
                call_deferred("set_new_scene_fader")
            "background":
                call_deferred("set_new_background")
        return resource
    else: # error during loading
        call_deferred('display_error', "Error during Loading! Error Number = " + str(err))

func display_error(error):
    PM.Settings.Log(error, "critical")
            
func update_progress(progress):
    PM.Settings.Log("Loading Progress = " + str(progress[0] * 100) + "%")

func prepare_new_scene(scene_resource):
    previous_scene = current_scene
    current_scene = scene_resource.instance()
    update_progress([0.999])
    call_deferred("set_new_scene_part2")

func set_new_scene(scene_resource=null):
    if scene_resource == null:
        scene_resource = threaded_scene_loader1.wait_to_finish()
    previous_scene = current_scene
    current_scene = scene_resource.instance()
    load_theme_details()
    update_progress([0.999])
    call_deferred("set_new_scene_part2")
    
func set_new_scene_part2():
#    while not SceneFader.dark:
#        yield(get_tree().create_timer(0.01), "timeout")
    if active_scene_container == 0:
        $Scene1.add_child(current_scene)
        active_scene_container = 1
    else:
        $Scene0.add_child(current_scene)
        active_scene_container = 0
    update_progress([1.0])
    # start to End your loading animation.
    get_tree().call_group("SceneFader", "fade_out")
    previous_scene.queue_free()
    PM.Settings.Session.set_data("Container_loading_state", "idle")
    
func set_new_background():
    var resource = threaded_scene_loader2.wait_to_finish().instance()
    $Background.add_child(resource)
    PM.Settings.Log("Loaded Background Resource: " + resource.name)
    finished_loading_thread2()
    
func finished_loading_thread2():
    loading_state2 = "idle"

func next_screen(menu):
    var xy = menu.split_floats(",")
    get_tree().call_group("Camera", "instant_pan_camera", int(xy[0]), int(xy[1]))
    if PM.Settings.Session.get_data("previous_menu") != PM.Settings.Session.get_data("current_screen"):
        if PM.Settings.Session.get_data("current_screen") != null:
            PM.Settings.Session.set_data("previous_menu", PM.Settings.Session.get_data("current_screen"))
    call_deferred("offset_UI_child", xy)
    PM.Settings.Session.set_data("current_screen", menu)
    
func offset_UI_child(offsets):
    if $UI.get_child_count() != 0:
        var calc_offset_x = offsets[0] * 540 * 1
        var calc_offset_y = offsets[1] * 960 * 1
        $UI.get_child(0).rect_position.x = calc_offset_x
        $UI.get_child(0).rect_position.y = calc_offset_y
        ui_child_offset = $UI.get_child(0).rect_position

func offset_UI_for_virtual_keyboard(keyboard_height):
    $UI.get_child(0).rect_position = ui_child_offset
    $UI.get_child(0).rect_position.y += keyboard_height
    
func load_theme_details():
    if PM.Settings.Preferences.get_data("ThemeName") == null:
        PM.Settings.Preferences.set_data("ThemeName", "default")
    var file = File.new()
    if file.file_exists("res://assets/themes/" + PM.Settings.Preferences.get_data(
            "ThemeName") + "/" + PM.Settings.Preferences.get_data("ThemeName") + 
            ".json"):
        file.open("res://assets/themes/" + PM.Settings.Preferences.get_data("ThemeName") + 
            "/" + PM.Settings.Preferences.get_data("ThemeName") + ".json", file.READ)
        var text = file.get_as_text()
        var theme = parse_json(text)
        file.close()
        for detail in theme:
            PM.Settings.Session.set_data(detail, theme[detail])
