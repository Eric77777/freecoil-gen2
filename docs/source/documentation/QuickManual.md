# Play Screen Overview (screen that is displayed to the user during gameplay)

## Header

### Menu Button

Contains the HUD Footer Layout button, Player Hit Indicator Duration button, and End Game (during gameplay only) button.  The HUD Footer Layout button provides 2 options for the general layout of the Footer during gameplay.  The Player Hit Indicator changes the duration of the visual feedback that the player receives when hit.

### GPS Icon

Shows the status of the gps location functionality.

### Game Network Icon

Shows the status of the user's connection to a current game.  When pressed, the button also shows the IP Address of the user.

### Recoil Weapon Icon

Shows the status of the Recoil Weapon connection (bluetooth).  When pressed, the button will cycle the recoil functionality of a connected firearm on/off (if cycling of the firearm is enabled).

### Recoil Weapon Battery Icon

Shows the status of the battery of the currently connected Recoil Weapon.

### Player Avatar

Shows the selected avatar of the user.

## Footer

### Show Scores Button

Shows the scores of the current game.

### Health Meter and Status (red)

Shows the health level of the user.

### Inventory Button (Rucksack image, when Supply Depot is enabled only)

Displays the inventory items of the user and enables the user to use the currently owned items.  The display/use of items is dependent on the user type (Soldier, Medic).

### Ammunition Meter button and Status (blue)

Shows the magazine ammo level of the currently used firearm.  When pressed, the button will reload the current firearm.  Additionally, the magazine button on the Recoil Weapon will reload the current firearm.

### Ammunition Box Button and Number

Shows the total ammo level of the currently used firearm.  When pressed, the button will also cycle through the available firearm firing modes (Single, Burst, Auto).  The status of the firing mode will display on the ammo box as a single bullet (single mode), three bullets (burst mode), or six bullets (automatic mode).  Additionally, the power button on the Recoil Weapon can be used to cycle through the firing modes.

### Firearm Button

Shows the currently used firearm name.  When pressed will display a selectable list of the user's currently owned firearms.

### Shield Meter and Status (green)

Shows the shield level of the user.

### Radio Comms Button and Message

When teams are enabled, will display the comms window to enable the player to send a series of preset messages to all teammates.

## Left Panel (left side of the map)

### Kills

Shows the number of kills for the user's current game.  When the player has killed 3 opponents or more, without having been killed themselves, the display will indicate that the player is "On Fire".

### In-Game Panel Display Button

Enables the player to show/hide any of the main display panels during gameplay.

## Right Panel (right side of the map)

### Deaths

Shows the number of deaths for the user's current game.

### Supply Depot and Currency (when Supply Depot is enabled and available only)

Shows the currency of the user.  When pressed, displays all of the available shops in the Supply Depot.
